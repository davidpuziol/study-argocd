# Study Argocd

Todo o estudo aqui é baseado na [documentação oficial](https://argo-cd.readthedocs.io/)

## O que é o ArgoCD e como funciona?

Básicamente o ArgoCD é um serviço que confere o seu cluster kuberentes com um repositório que contém os manifestos e caso esteja diferente ele muda automáticamente. É uma ferramenta de GitOps (onde o repositório git é a forte da verdade) para kubernetes, ou seja o estado do cluster precisa estar igual o do repo.

Um manifesto pode ser aplicado no kubernetes de diferentes maneiras, mas as principais são:

- Usando o [kustomize](https://kustomize.io/)
- Utilizando o [Helm](https://helm.sh/)
- Criando um apply de seus próprios manifestos

O Argo CD automatiza a implantação dos estados de serviços nos destino especificado, como se fosse o um state te terraform, mas para isso ele fica monitorando (rastreada) de tempos em tempos. Não somente para a branch main, mas qualquer uma branch ou tag que esteja sendo rastreada (tracked). O tempo com que o argocd confere o estado também é configurado para cada item, podendo por exemplo na branch main ser colocado de 5 em 5 minutos, ou até menos, e na branch release 15 em 15.

Um aplicativo implementado cujo estado ao vivo se desvia do estado de destino é considerado OutOfSync. O Argo CD relata e visualiza as diferenças, ao mesmo tempo em que fornece recursos para sincronizar automaticamente ou manualmente o estado ao vivo de volta ao estado de destino desejado. Quaisquer modificações feitas no estado de destino desejado no repositório Git podem ser aplicadas automaticamente e refletidas nos ambientes de destino especificados

Funcionalidades:

- Implantação automatizada de aplicativos para ambientes de destino especificados
- Suporte para várias ferramentas de gerenciamento/templating de configuração (Kustomize, Helm, Jsonnet, plain-YAML)
- Capacidade de gerenciar e implantar em vários clusters
- Integração SSO (OIDC, OAuth2, LDAP, SAML 2.0, GitHub, GitLab, Microsoft, LinkedIn)
- Políticas de multilocação e RBAC para autorização
- Rollback/Roll-anywhere para qualquer configuração de aplicativo confirmada no repositório Git
- Análise do status de integridade dos recursos do aplicativo
- Detecção e visualização automática de desvio de configuração
- Sincronização automática ou manual de aplicativos para o estado desejado
- IU da Web que fornece visualização em tempo real da atividade do aplicativo
- CLI para automação e integração CI
- Integração de webhook (GitHub, BitBucket, GitLab)
- Tokens de acesso para automação
- Ganchos de pré-sincronização, sincronização e pós-sincronização para oferecer suporte a lançamentos de aplicativos complexos (por exemplo, atualizações de azul/verde e canário)
- Trilhas de auditoria para eventos de aplicativos e chamadas de API
- Métricas do Prometheus
- Substituições de parâmetros para substituir parâmetros do helm no Git

## Arquitetura do Argo CD

O argocd implementado dentro de um cluster kubernetes não necessáriamente faz o seu serviço para aquele cluster, mas qualquer um que nele esteja configurado. Além disso ele prove uma interface gráfica e uma cli para iteração ou até mesmo automação de suas configurações.

![arch](./pictures/architecture.webp)

Os componentes do são:

- API-Server
  - É um gRPC/Rest server que são consumidos pelo cli e web gui. 
    - Gerencia apps e relatórios
    - Invoca operação para sync, reverse e outras ações definidas pelo usuário
    - Repositório e gerenciamento de credenciais armazenadas no secret
    - Autenticação e auth vindo de outros providers
    - Força o RBAC
    - Escuta os eventos de webhook vindos do git

- Repository Server
  - O servidor de repositório é um serviço interno que mantém um cache local do repositório Git contendo os manifestos do aplicativo. Ele é responsável por gerar e retornar os manifestos do Kubernetes quando fornecidos as seguintes entradas:
    - URL do respositório
    - revisão (commit, tag, branch)
    - path do app
    - configurações específicas do modelo: params, helm values
  - Utiliza o Redis

- App Controller
  - O controlador de aplicativo é um controlador Kubernetes que monitora continuamente os aplicativos em execução e compara o estado ativo atual com o estado de destino desejado (conforme especificado no repositório). Ele detecta OutOfSynco estado do aplicativo e, opcionalmente, executa ações corretivas. Ele é responsável por invocar quaisquer ganchos definidos pelo usuário para eventos de ciclo de vida (Pré-sincronização, Sincronização, Pós-sincronização)


## Como seguir pelo estudo:

[Demo](https://cd.apps.argoproj.io/)\
[Instalação](./manuscritos/1%20Instalation.md)\
[Instalação Avançada](./manuscritos/2%20Advanced-install.md)\
[Instalação](./manuscritos/instalation.md)\
[Instalação](./manuscritos/instalation.md)\
[Instalação](./manuscritos/instalation.md)\
[Instalação](./manuscritos/instalation.md)\
[Instalação](./manuscritos/instalation.md)
