# Instalação

<https://argo-cd.readthedocs.io/en/stable/operator-manual/installation/>

## Pre Requisitos

Como tudo no kubernetes é necessário o [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) instalado e um cluster previamente configurado.

Também é necessário um service discovery como o CoreDNS instalado no cluster.

Se vc tiver rodando por exemplo um k3d por exemplo ele já vem instalado. Confira no seu cluster se existe algum service discovery já instalado.

## Tipos de Instalação

- Multi Tenant
  - Usado para atender várias equipes de desenvolvimento e é mantido por uma equipe de plataform
  - Pode usar tanto a gui quando o cli
- Core
  - Mais indicados para administradores de cluster e não precisam de multi tenant.
  - Instala menos componentes
  - Mais fácil de configurar
  - O usuário precisa ter acesso ao kubernetes para gerenciar o Argo CD

No estudo vamos focar no completo, Multi Tenant
dentro desse modelo tempos

- sem HA
  - Usado para testes
- com HA
  - Recomendado para uso em produção
  - Possue os mesmo componentes, mas ajustados para alta disponibilidade e resiliência (Vamos testar este)

Dois manifestos existem:

- install.yaml (Vamos usar esta)
  - Instalação padrão com acesso de admin do cluster
  - Use se for implantar apps no mesmo cluster que o argocd esta instalado
  - Ainda pode implantar apps em outros clusters com as credenciais corretas

- namespace-install.yaml
  - Use caso tenha tenha várias instancias do argocd para equipes diferentes, onde cada instancia implementa apps em clusters externos.
  - Ainda é possível implantar no mesmo cluster com as credencias corretas.

Aplicando os manifestos do argo em um cluster k3d local. Com a configuração abaixo já estamos configurando a porta do ingress para depois resolver.

````bash
#Instalar o k3d
curl -s https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash

#criar o cluster com 3 nodes workers
k3d cluster create  -p "8081:80@loadbalancer" --agents 3

k get nodes
NAME                       STATUS   ROLES                  AGE   VERSION
k3d-k3s-default-server-0   Ready    control-plane,master   73m   v1.24.4+k3s1
k3d-k3s-default-agent-2    Ready    <none>                 73m   v1.24.4+k3s1
k3d-k3s-default-agent-0    Ready    <none>                 73m   v1.24.4+k3s1
k3d-k3s-default-agent-1    Ready    <none>                 73m   v1.24.4+k3s1
````

Vamos instalar o argo no namespace argocd

````bash
# Cria o namespace
kubectl create namespace argocd
# Download de todos os manifestos
wget  https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/ha/install.yaml
# Aplicando o manifesto
kubectl apply -n argocd -f install.yaml
...
# Conferindo os pods
k get pods -n argocd
NAME                                                READY   STATUS    RESTARTS   AGE
argocd-redis-ha-haproxy-59b5d8568b-9t6pz            1/1     Running   0          4m46s
argocd-redis-ha-haproxy-59b5d8568b-5gpdl            1/1     Running   0          4m46s
argocd-redis-ha-haproxy-59b5d8568b-dqjsw            1/1     Running   0          4m46s
argocd-applicationset-controller-66b744f75b-bgmql   1/1     Running   0          4m46s
argocd-notifications-controller-69996dd789-pjvq5    1/1     Running   0          4m46s
argocd-dex-server-d5848bfff-r7tmh                   1/1     Running   0          4m46s
argocd-application-controller-0                     1/1     Running   0          4m46s
argocd-server-7896cfdbb7-vb7jn                      1/1     Running   0          4m46s
argocd-repo-server-76559b644c-d2m7r                 1/1     Running   0          4m46s
argocd-server-7896cfdbb7-jtz7t                      1/1     Running   0          4m45s
argocd-repo-server-76559b644c-wmtfh                 1/1     Running   0          4m46s
argocd-redis-ha-server-0                            3/3     Running   0          4m25s
argocd-redis-ha-server-1                            3/3     Running   0          3m9s
argocd-redis-ha-server-2                            3/3     Running   0          114s
````

Faça um port forward para conseguir acessar o serviço inicialmente, depois vamos configurar um ingress.

````bash
kubectl port-forward svc/argocd-server -n argocd 8080:443
````

Acesse o navegador em <https://localhost:8080>
![argo](../pictures/argo.png)

Pegando o password do user admin para logar tantona web ou no cli mais adiante

````bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo

1DN4VBNo1oHYHtl-
````

![login](../pictures/argologed.png)

## Instação usando o helm

Eu particulamente prefiro este método


````bash
# vamos adicionar o repositório e fazer update
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update

# Vamos baixar o values para ver o que podemos mudar caso queira

helm show values argo/argo-cd > manifests/Values.yaml

# Se quiser alterar algum parâmtro da instalação edite antes de aplicar o arquivo Values.yaml, por exemplo seria possível já alterar a senha.
helm install argocd argo/argo-cd -f ./manifests/Values.yaml --create-namespace --namespace=argocd

# Pegando a senha
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d; echo
# BfUT8kID-g2UDN7G

k get all 
# NAME                                                    READY   STATUS    RESTARTS   AGE
# pod/argocd-redis-8f968f7fd-vptsg                        1/1     Running   0          5m21s
# pod/argocd-notifications-controller-5d6bd4b7c-c246v     1/1     Running   0          5m21s
# pod/argocd-applicationset-controller-7bccc5f68f-27kxz   1/1     Running   0          5m20s
# pod/argocd-dex-server-f5d7684dd-h45cc                   1/1     Running   0          5m21s
# pod/argocd-server-6469848f7f-gmc96                      1/1     Running   0          5m21s
# pod/argocd-application-controller-0                     1/1     Running   0          5m21s
# pod/argocd-repo-server-5c57d74544-24g8m                 1/1     Running   0          5m21s

# NAME                                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)             AGE
# service/argocd-redis                       ClusterIP   10.43.163.229   <none>        6379/TCP            5m21s
# service/argocd-server                      ClusterIP   10.43.113.40    <none>        80/TCP,443/TCP      5m21s
# service/argocd-dex-server                  ClusterIP   10.43.191.194   <none>        5556/TCP,5557/TCP   5m21s
# service/argocd-repo-server                 ClusterIP   10.43.228.76    <none>        8081/TCP            5m21s
# service/argocd-applicationset-controller   ClusterIP   10.43.76.200    <none>        7000/TCP            5m21s

# NAME                                               READY   UP-TO-DATE   AVAILABLE   AGE
# deployment.apps/argocd-redis                       1/1     1            1           5m21s
# deployment.apps/argocd-notifications-controller    1/1     1            1           5m21s
# deployment.apps/argocd-applicationset-controller   1/1     1            1           5m21s
# deployment.apps/argocd-dex-server                  1/1     1            1           5m21s
# deployment.apps/argocd-server                      1/1     1            1           5m21s
# deployment.apps/argocd-repo-server                 1/1     1            1           5m21s

# NAME                                                          DESIRED   CURRENT   READY   AGE
# replicaset.apps/argocd-redis-8f968f7fd                        1         1         1       5m21s
# replicaset.apps/argocd-notifications-controller-5d6bd4b7c     1         1         1       5m21s
# replicaset.apps/argocd-applicationset-controller-7bccc5f68f   1         1         1       5m21s
# replicaset.apps/argocd-dex-server-f5d7684dd                   1         1         1       5m21s
# replicaset.apps/argocd-server-6469848f7f                      1         1         1       5m21s
# replicaset.apps/argocd-repo-server-5c57d74544                 1         1         1       5m21s

# NAME                                             READY   AGE
# statefulset.apps/argocd-application-controller   1/1     5m21s

kubectl port-forward service/argocd-server -n argocd 8080:443
````

Agora é só acessar o browser em localhost:8080 e colocar o user admin e a senha gerada.

## Instalando o ArgoCD CLI

Na página do projeto voce pode baixar direto o binário e colocálo em um local onde já seu path já esteja rodando.

````bash
wget https://github.com/argoproj/argo-cd/releases/download/v2.5.5/argocd-linux-amd64
chmod -x argocd-linux-amd64 
sudo cp argocd-linux-amd64 /usr/local/bin/argocd
argocd version
````

Caso tenha o brew instalado

````bash
brew install argocd
````

Caso utilize o asdf

````bash
asdf plugin-add argocd
asdf install argo latest
asdf global argocd
````

Depois de estar com o argocd cli instalado e o port forward directionado use o mesmo password do secret

````bash
argocd login localhost:8080

WARNING: server certificate had error: x509: certificate signed by unknown authority. Proceed insecurely (y/n)? y
Username: admin
Password: 
'admin:login' logged in successfully
Context 'localhost:8080' updated
````
